# FunGameRefresh

好玩的下拉刷新控件，让我们一起来回味童年。目前支持两种游戏：打砖块和打坦克。

# 效果图
<img src="https://gitee.com/HarmonyOS-tpc/FunGameRefresh/raw/master/screenshot/Screenshot_2.gif" width="40%">  
<img src="https://gitee.com/HarmonyOS-tpc/FunGameRefresh/raw/master/screenshot/Screenshot_1.gif" width="40%">

# 导入项目
 通过library生成har包，添加har包到libs文件夹内
 在entry的gradle内添加如下代码

 ```gradle
 implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 ```
 
 方法二：
 
 ```gradle
 allprojects{
     repositories{
         mavenCentral()
     }
 }
 implementation 'io.openharmony.tpc.thirdlib:FunGameRefresh:1.0.2'
 ```

# 用法
 ### `XML` init:
打坦克游戏例子
```xml
  <com.hitomi.refresh.view.FunGameRefreshView
            ohos:id="$+id:refresh_fun_game"
            ohos:height="match_parent"
            ohos:width="match_parent"
            hap:game_type="1"
            hap:top_text_size="20fp"
            hap:bottom_text_size="20fp">

        <ListContainer
            ohos:id="$+id:list_view"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="#ffffff"/>
        </com.hitomi.refresh.view.FunGameRefreshView>
```
打砖块游戏例子（在xml里设置text文字内容时，主动换行只能使用`&#x000A;`，而不是\n）
```xml
<com.hitomi.refresh.view.FunGameRefreshView
        ohos:id="$+id:refresh_fun_game"
        ohos:height="match_parent"
        ohos:width="match_parent"
        hap:ball_speed="fast"
        hap:block_horizontal_num="6"
        hap:bottom_text_size="18fp"
        hap:game_type="0"
        hap:mask_bottom_text="快来玩游戏&#x000A;上下滑动控制游戏"
        hap:mask_top_text="下拉刷新"
        hap:top_text_size="18fp">

        <ListContainer
            ohos:id="$+id:list_view"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="#ffffff"/>
    </com.hitomi.refresh.view.FunGameRefreshView>
```

### 初始化控件，设置文字描述
```java
 refreshView = (FunGameRefreshView) findComponentById(ResourceTable.Id_refresh_fun_game);
 refreshView.setLoadingText("玩个游戏解解闷");
 refreshView.setGameOverText("游戏结束");
 refreshView.setLoadingFinishedText("加载完成");
 refreshView.setTopMaskText("下拉刷新");
 refreshView.setBottomMaskText("上下滑动控制游戏");
```


### 监听刷新事件
```java
refreshView.setOnRefreshListener(new FunGameRefreshView.FunGameRefreshListener() {
            @Override
            public void onPullRefreshing() {
            }

            @Override
            public void onRefreshComplete() {
                updateDataList();
                provider.notifyDataChanged();
            }
        });
```
# 自定义属性
| 属性          | 描述 |
|:---				 |:---|
| left_model_color 	     | 动画左边部位颜色 |
| middle_model_color	     | 动画中间运动的小球颜色 |
| right_model_color 	     |动画右边部位颜色 |
| game_type 	     | 游戏种类（0为打砖块，1为打坦克） |
| mask_top_text 	     | 上边帷幕中的文字（在xml里设置text文字内容时，主动换行只能使用`&#x000A;`，而不是\n） |
| mask_bottom_text 	     | 下边帷幕中的文字 （在xml里设置text文字内容时，主动换行只能使用`&#x000A;`，而不是\n）|
| text_loading  	     | 加载开始文字 |
| text_loading_finished 	     | 加载结束文字 |
| text_game_over 	     | 游戏结束文字|
| top_text_size 	     | 上边帷幕中的文字大小 |
| bottom_text_size	     | 下边帷幕中的文字大小 |
| block_horizontal_num	     | 挡板横向排列的数目 |
| ball_speed	     | 小球速度（默认为medium(6),速度为固定的三种：low、medium、fast） |

# Licence
MIT