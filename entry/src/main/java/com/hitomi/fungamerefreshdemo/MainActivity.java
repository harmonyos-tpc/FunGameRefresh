package com.hitomi.fungamerefreshdemo;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * Created by hitomi on 2016/12/2.
 */
public class MainActivity extends BaseActivity implements Component.ClickedListener {
    private Button btnListView;
    private Button btnGridView;

    @Override
    public void setContentView() {
        setUIContent(ResourceTable.Layout_activity_main);
    }

    @Override
    public void initView() {
        btnListView = (Button) findComponentById(ResourceTable.Id_btn_list_view);
        btnGridView = (Button) findComponentById(ResourceTable.Id_btn_grid_view);
    }

    @Override
    public void setViewListener() {
        btnListView.setClickedListener(this);
        btnGridView.setClickedListener(this);
    }

    @Override
    public void processLogic() {
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_list_view:
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ListViewActivity.class.getCanonicalName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                break;
            case ResourceTable.Id_btn_grid_view:
                Intent intent1 = new Intent();
                Operation operation1 = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(GridViewActivity.class.getCanonicalName())
                        .build();
                intent1.setOperation(operation1);
                startAbility(intent1);
                break;
        }
    }
}
