package com.hitomi.fungamerefreshdemo;

import com.hitomi.refresh.view.FunGameRefreshView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;

/**
 * Created by hitomi on 2016/12/2.
 */
public abstract class BaseActivity extends Ability {
    /**
     * 刷新控件
     */
    protected FunGameRefreshView refreshView;

    /**
     * 列表数据
     */
    protected ArrayList<String> dataList;

    /**
     * 设置layout
     */
    public abstract void setContentView();

    /**
     * 初始化ui
     */
    public abstract void initView();

    /**
     * 设置监听
     */
    public abstract void setViewListener();

    /**
     * 其他逻辑
     */
    public abstract void processLogic();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setContentView();
        initView();
        setViewListener();
        processLogic();
    }

    /**
     * 数据
     * @return 列表数据
     */
    protected ArrayList<String> createDate() {
        dataList = new ArrayList<>();
        dataList.add("A");
        dataList.add("B");
        dataList.add("C");
        dataList.add("D");
        dataList.add("E");
        dataList.add("F");
        dataList.add("G");
        dataList.add("H");
        dataList.add("I");
        dataList.add("J");
        dataList.add("K");
        dataList.add("L");
        dataList.add("M");
        dataList.add("N");
        dataList.add("O");
        dataList.add("P");
        return dataList;
    }

    /**
     * 更新数据
     */
    protected void updateDataList() {
        String lastStr = dataList.get(dataList.size() - 1);
        char lastChar = lastStr.toCharArray()[0];
        int ch = (int) lastChar;
        ++ch;
        String str = String.valueOf((char) ch);
        dataList.add(str);
    }
}
