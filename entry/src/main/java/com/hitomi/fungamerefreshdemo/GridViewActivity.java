package com.hitomi.fungamerefreshdemo;

import com.hitomi.refresh.view.FunGameRefreshView;

import ohos.agp.components.ListContainer;

/**
 * Created by hitomi on 2016/12/2.
 */
public class GridViewActivity extends BaseActivity {
    private ListContainer gridView;
    private EasyGridProvider provider;

    @Override
    public void setContentView() {
        setUIContent(ResourceTable.Layout_ability_grid);
    }

    @Override
    public void initView() {
        refreshView = (FunGameRefreshView) findComponentById(ResourceTable.Id_refresh_fun_game);
        gridView = (ListContainer) findComponentById(ResourceTable.Id_list_view);
    }

    @Override
    public void setViewListener() {
        refreshView.setOnRefreshListener(new FunGameRefreshView.FunGameRefreshListener() {
            @Override
            public void onPullRefreshing() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
            }

            @Override
            public void onRefreshComplete() {
                updateDataList();
                provider.notifyDataChanged();
            }
        });
    }

    @Override
    public void processLogic() {
        provider = new EasyGridProvider<String>(getContext(),
                ResourceTable.Layout_list_item, createDate()) {
            @Override
            protected void bind(ViewHolder holder, String data, int position) {
                holder.setText(ResourceTable.Id_text, data);
            }
        };
        provider.setNumColumns(3);
        gridView.setItemProvider(provider);
    }
}
