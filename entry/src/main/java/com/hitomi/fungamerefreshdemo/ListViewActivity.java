package com.hitomi.fungamerefreshdemo;

import com.hitomi.refresh.view.FunGameRefreshView;

import ohos.agp.components.ListContainer;

/**
 * Created by hitomi on 2016/12/2.
 */
public class ListViewActivity extends BaseActivity {
    private ListContainer listView;
    private EasyGridProvider provider;

    @Override
    public void setContentView() {
        setUIContent(ResourceTable.Layout_ability_list);
    }

    @Override
    public void initView() {
        refreshView = (FunGameRefreshView) findComponentById(ResourceTable.Id_refresh_fun_game);
        refreshView.setLoadingText("玩个游戏解解闷");
        refreshView.setGameOverText("游戏结束");
        refreshView.setLoadingFinishedText("加载完成");
        refreshView.setTopMaskText("下拉刷新");
        refreshView.setBottomMaskText("上下滑动控制游戏");

        listView = (ListContainer) findComponentById(ResourceTable.Id_list_view);
    }

    @Override
    public void setViewListener() {
        refreshView.setOnRefreshListener(new FunGameRefreshView.FunGameRefreshListener() {
            @Override
            public void onPullRefreshing() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
            }

            @Override
            public void onRefreshComplete() {
                updateDataList();
                provider.notifyDataChanged();
            }
        });
    }

    @Override
    public void processLogic() {
        provider = new EasyGridProvider<String>(getContext(),
                ResourceTable.Layout_list_item, createDate()) {
            @Override
            protected void bind(ViewHolder holder, String data, int position) {
                holder.setText(ResourceTable.Id_text, data);
            }
        };
        listView.setItemProvider(provider);
    }

}
