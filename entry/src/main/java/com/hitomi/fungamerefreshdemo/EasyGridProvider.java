/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hitomi.fungamerefreshdemo;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.HashMap;
import java.util.List;

import static ohos.agp.utils.LayoutAlignment.TOP;

/**
 * ListContainer适配器_网格
 *
 * @param <T> 数据类型
 */
public abstract class EasyGridProvider<T> extends RecycleItemProvider {
    private Context context;
    private List<T> data;
    private int mLayoutId;
    private int numColumns = 1;
    private OnItemClickListener onItemClickListener;

    /**
     * 条目点击事件监听
     */
    public static abstract class OnItemClickListener {
        /**
         * 条目点击事件监听
         *
         * @param component 被点击的控件
         * @param position  索引
         */
        public abstract void onItemClick(Component component, int position);
    }

    /**
     * @param context   上下文
     * @param data      数据源
     * @param mLayoutId 条目的资源文件id
     * @return 适配器对象
     */
    public EasyGridProvider(Context context, int mLayoutId, List<T> data) {
        this.context = context;
        this.data = data;
        this.mLayoutId = mLayoutId;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    public void setData(List<T> data) {
        this.data = data;
        notifyDataChanged();
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size() % numColumns == 0 ? data.size() / numColumns : data.size() / numColumns + 1;
        } else {
            return 0;
        }
    }

    @Override
    public T getItem(int position) {
        return data != null ? data.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        ViewHolder viewHolder;
        convertComponent = new DirectionalLayout(context);
        ((DirectionalLayout) convertComponent).setOrientation(Component.HORIZONTAL);
        ComponentContainer.LayoutConfig layoutConfig = convertComponent.getLayoutConfig();
        layoutConfig.width = DependentLayout.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = DependentLayout.LayoutConfig.MATCH_CONTENT;
        convertComponent.setLayoutConfig(layoutConfig);
        for (int i = 0; i < numColumns; i++) {
            if (position * numColumns + i < data.size()) {
                DirectionalLayout dlItemParent = new DirectionalLayout(context);
                dlItemParent.setLayoutConfig(new DirectionalLayout.LayoutConfig(0,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT, TOP, 1));
                Component childConvertComponent = LayoutScatter.getInstance(context).parse(mLayoutId, null, false);
                int finalI = i;
                childConvertComponent.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(component, position * numColumns + finalI);
                        }
                    }
                });
                dlItemParent.addComponent(childConvertComponent);
                ((ComponentContainer) convertComponent).addComponent(dlItemParent);
                viewHolder = new ViewHolder(childConvertComponent);
                bind(viewHolder, getItem(position * numColumns + i), position * numColumns + i);
            } else {
                // 用Component占位会导致高度为MATCH_PARENT,所以此处用DirectionalLayout占位
                DirectionalLayout childConvertComponent = new DirectionalLayout(context);
                childConvertComponent.setLayoutConfig(new DirectionalLayout.LayoutConfig(0,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT, TOP, 1));
                ((ComponentContainer) convertComponent).addComponent(childConvertComponent);
            }
        }
        return convertComponent;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * 每个条目需要执行的逻辑方法
     *
     * @param holder   条目绑定的holder对象
     * @param itemData 条目对应的数据
     * @param position 条目的索引
     */
    protected abstract void bind(ViewHolder holder, T itemData, int position);

    /**
     * 绑定条目控件的自定义holder类
     */
    protected static class ViewHolder {
        /**
         * 绑定的条目控件
         */
        public Component itemView;
        private HashMap<Integer, Component> mViews = new HashMap<>();

        ViewHolder(Component component) {
            this.itemView = component;
        }

        /**
         * 链式调用，用于给Text控件快速设置文本
         *
         * @param viewId Text控件的资源id
         * @param text   需要设置的文本
         * @return holder对象自身
         */
        public ViewHolder setText(int viewId, String text) {
            ((Text) getView(viewId)).setText(text);
            return this;
        }

        /**
         * 根据id或者条目中的某个控件
         *
         * @param viewId 需要获取的控件的资源id
         * @param <E>    需要获取控件的类型
         * @return 需要获取的控件
         */
        public <E extends Component> E getView(int viewId) {
            E view = (E) mViews.get(viewId);
            if (view == null) {
                view = (E) itemView.findComponentById(viewId);
                mViews.put(viewId, view);
            }
            return view;
        }
    }
}
