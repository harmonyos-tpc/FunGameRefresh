/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hitomi.refresh.view;

/**
 * 颜色工具类
 */
public class ColorUtils {
    /**
     * the red component of a color int
     *
     * @param color color
     * @return the red component of a color int. This is the same as saying
     */
    public static int red(int color) {
        return (color >> 16) & 0xFF;
    }

    /**
     * the green component of a color int
     *
     * @param color color
     * @return the green component of a color int. This is the same as saying
     */
    public static int green(int color) {
        return (color >> 8) & 0xFF;
    }

    /**
     * the blue component of a color int
     *
     * @param color color
     * @return the blue component of a color int. This is the same as saying
     */
    public static int blue(int color) {
        return color & 0xFF;
    }

}
