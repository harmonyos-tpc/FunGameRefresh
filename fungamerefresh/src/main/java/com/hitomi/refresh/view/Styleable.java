/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hitomi.refresh.view;

/**
 * 属性名
 */
public class Styleable {
    /**
     * 动画左边部位颜色
     */
    public static final String LEFT_MODEL_COLOR = "left_model_color";

    /**
     * 动画中间小球颜色
     */
    public static final String MIDDLE_MODEL_COLOR = "middle_model_color";

    /**
     * 动画右边部位颜色
     */
    public static final String RIGHT_MODEL_COLOR = "right_model_color";

    /**
     * 游戏种类
     */
    public static final String GAME_TYPE = "game_type";

    /**
     * 上边帷幕中的文字
     */
    public static final String MASK_TOP_TEXT = "mask_top_text";

    /**
     * 下边帷幕中的文字
     */
    public static final String MASK_BOTTOM_TEXT = "mask_bottom_text";

    /**
     * 加载开始文字
     */
    public static final String TEXT_LOADING = "text_loading";

    /**
     * 加载结束文字
     */
    public static final String TEXT_LOADING_FINISHED = "text_loading_finished";

    /**
     * 游戏结束文字
     */
    public static final String TEXT_GAME_OVER = "text_game_over";

    /**
     * 上边帷幕中的文字大小
     */
    public static final String TOP_TEXT_SIZE = "top_text_size";

    /**
     * 下边帷幕中的文字大小
     */
    public static final String BOTTOM_TEXT_SIZE = "bottom_text_size";

    /**
     * 挡板长度
     */
    public static final String BLOCK_HORIZONTAL_NUM = "block_horizontal_num";

    /**
     * 小球速度
     */
    public static final String BALL_SPEED = "ball_speed";
}
