package com.hitomi.refresh.view;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 * Created by Hitomis on 2016/3/10.
 * email:196425254@qq.com
 */
public class FunGameFactory {
    // Refused to use enum
    static final int HITBLOCK = 0;

    static final int BATTLECITY = 1;

    static FunGameView createFunGameView(Context context, AttrSet attrSet, int type) {
        FunGameView funGameView = null;
        switch (type) {
            case HITBLOCK:
                funGameView = new HitBlockView(context, attrSet);
                break;
            case BATTLECITY:
                funGameView = new BattleCityView(context, attrSet);
                break;
            default:
                funGameView = new HitBlockView(context, attrSet);
        }
        return funGameView;
    }
}
